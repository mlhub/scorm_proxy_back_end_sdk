<?php

namespace Mlh\ScormProxySdk;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Mlh\ScormProxySdk\Configs\Interfaces\SettingsRepository;
use Mlh\ScormProxySdk\Exceptions\RefreshTokenFailedException;
use Mlh\ScormProxySdk\Exceptions\SendWebhookFailedException;
use Psr\Http\Message\ResponseInterface;

class ApiClient implements Interfaces\ApiClient
{
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;
    /**
     * @var Client
     */
    private $apiClient;
    /**
     * @var string
     */
    private $apiUrl;

    /**
     * ApiClient constructor.
     * @param SettingsRepository $settingsRepository
     * @param Client $apiClient
     */
    public function __construct(SettingsRepository $settingsRepository, Client $apiClient)
    {
        $this->settingsRepository = $settingsRepository;
        $this->apiClient = $apiClient;

        $this->apiUrl = $settingsRepository->getScormProxyUrl();
    }

    /**
     *
     * @throws RefreshTokenFailedException
     */
    private function refreshAccessToken(): void
    {
        try {
            $response = $this->apiClient->post(
                $this->apiUrl . '/api/auth/login' ,
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                    'json' => [
                        'client_id' => $this->settingsRepository->getClientId(),
                        'client_secret' => $this->settingsRepository->getClientSecret(),
                    ],
                ]
            );
            $parsedResponse = json_decode($response->getBody()->getContents(), true);

            $this->settingsRepository->setAccessToken($parsedResponse['access_token'], Carbon::parse($parsedResponse['expires_at']));
        } catch (ClientException $exception) {
            throw new RefreshTokenFailedException($exception);
        }
    }

    /**
     * @param string $scormUrl
     * @throws SendWebhookFailedException
     * @throws RefreshTokenFailedException
     */
    public function sendNewScormWebhook(string $scormUrl): void
    {
        $this->sendPostRequest('/api/webhooks/new_scorm', ['url' => $scormUrl]);
    }

    /**
     * @param string $scormUrl
     * @throws SendWebhookFailedException
     * @throws RefreshTokenFailedException
     */
    public function sendScormDeletedWebhook(string $scormUrl): void
    {
        $this->sendPostRequest('/api/webhooks/scorm_deleted', ['url' => $scormUrl]);
    }

    /**
     * @param string $url
     * @param array $data
     * @return ResponseInterface
     * @throws RefreshTokenFailedException
     * @throws SendWebhookFailedException
     */
    private function sendPostRequest(string $url, array $data): ResponseInterface
    {
        if ($this->settingsRepository->getAccessTokenExpiresAt() <= Carbon::now()) {
            $this->refreshAccessToken();
        }

        try {
            return $this->apiClient->post(
                $this->apiUrl . $url,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->settingsRepository->getAccessToken(),
                        'Accept' => 'application/json',
                    ],
                    'json' => $data,
                ]
            );
        } catch (ClientException $exception) {
            if ($exception->getCode() === 401) {
                $this->refreshAccessToken();
                return $this->sendPostRequest($url, $data);
            }

            throw new SendWebhookFailedException($exception);
        }
    }
}
