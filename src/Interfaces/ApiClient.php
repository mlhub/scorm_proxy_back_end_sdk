<?php

namespace Mlh\ScormProxySdk\Interfaces;

interface ApiClient
{
    /**
     * @param string $scormUrl
     */
    public function sendNewScormWebhook(string $scormUrl): void;

    /**
     * @param string $scormUrl
     */
    public function sendScormDeletedWebhook(string $scormUrl): void;
}
