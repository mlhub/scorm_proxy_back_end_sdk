<?php

return [
    'client_id' => env('SCORM_PROXY_CLIENT_ID'),
    'client_secret' => env('SCORM_PROXY_CLIENT_SECRET'),
    'url' => env('SCORM_PROXY_CLIENT_URL'),
];
