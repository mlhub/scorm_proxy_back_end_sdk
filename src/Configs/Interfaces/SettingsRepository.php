<?php

namespace Mlh\ScormProxySdk\Configs\Interfaces;

use Carbon\Carbon;

interface SettingsRepository
{
    /**
     * @return string
     */
    public function getClientId(): string;

    /**
     * @return string
     */
    public function getClientSecret(): string;

    /**
     * @return string
     */
    public function getScormProxyUrl(): string;

    /**
     * @return string
     */
    public function getAccessToken(): ?string;

    /**
     * @param string $token
     * @param Carbon $expiresAt
     * @return void
     */
    public function setAccessToken(string $token, Carbon $expiresAt): void;

    /**
     * @return Carbon|null
     */
    public function getAccessTokenExpiresAt(): ?Carbon;
}
