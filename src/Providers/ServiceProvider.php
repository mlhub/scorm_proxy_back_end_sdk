<?php


namespace Mlh\ScormProxySdk\Providers;


use Mlh\ScormProxySdk\Interfaces\ApiClient;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        parent::register();

        $this->publishes([
            __DIR__ . '/../Configs/config.php' => config_path('scorm_proxy.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Configs/config.php', 'scorm_proxy'
        );

        $this->registerInDIContainer();
    }

    /**
     *
     */
    private function registerInDIContainer(): void
    {
        $this->app->bind(ApiClient::class, \Mlh\ScormProxySdk\ApiClient::class);
    }
}
