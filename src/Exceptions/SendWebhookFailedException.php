<?php

namespace Mlh\ScormProxySdk\Exceptions;

use Exception;
use Throwable;

class SendWebhookFailedException extends Exception
{
    /**
     * SendWebhookFailedException constructor.
     * @param Throwable $previous
     */
    public function __construct(Throwable $previous)
    {
        parent::__construct('Sending following webhook failed: ' . $previous->getMessage(), $previous->getCode(), $previous);
    }
}
