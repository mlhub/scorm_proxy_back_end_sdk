<?php

namespace Mlh\ScormProxySdk\Exceptions;

use Exception;
use Throwable;

class RefreshTokenFailedException extends Exception
{
    /**
     * RefreshTokenFailedException constructor.
     * @param Throwable $previous
     */
    public function __construct(Throwable $previous)
    {
        parent::__construct('Refresh scorm proxy token failed with message: ' . $previous->getMessage(), 401, $previous);
    }
}
