<?php

namespace Mlh\ScormProxySdk\Tests\TestUtils;

use Carbon\Carbon;
use Mlh\ScormProxySdk\Configs\Interfaces\SettingsRepository;

class TestSettingsRepository implements SettingsRepository
{
    /**
     * @var string
     */
    private $token = 'valid token';
    /**
     * @var Carbon
     */
    private $expiresAt;
    /**
     * @var string
     */
    private $clientId = 'client_id';
    /**
     * @var string
     */
    private $clientSecret = 'client_secret';

    /**
     * TestSettingsRepository constructor.
     * @param string|null $token
     * @param Carbon|null $expiresAt
     * @param int|null $clientId
     * @param int|null $clientSecret
     */
    public function __construct(?string $token = null, ?Carbon $expiresAt = null, ?int $clientId = null, ?int $clientSecret = null)
    {
        $this->token = $token;
        $this->expiresAt = $expiresAt;
        $this->clientId = $clientId ?? $this->clientId;
        $this->clientSecret = $clientSecret ?? $this->clientSecret;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function getScormProxyUrl(): string
    {
        return 'https://scorm-proxy.test';
    }

    public function getAccessToken(): ?string
    {
        return $this->token;
    }

    public function setAccessToken(string $token, Carbon $expiresAt): void
    {
        $this->token = $token;
        $this->expiresAt = $expiresAt;
    }

    public function getAccessTokenExpiresAt(): ?Carbon
    {
        return $this->expiresAt;
    }
}
