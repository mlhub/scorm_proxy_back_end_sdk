<?php

namespace Mlh\ScormProxySdk\Tests\ApiClient;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use Mlh\ScormProxySdk\ApiClient;
use Mlh\ScormProxySdk\Exceptions\RefreshTokenFailedException;
use Mlh\ScormProxySdk\Exceptions\SendWebhookFailedException;
use Mlh\ScormProxySdk\Tests\TestUtils\TestSettingsRepository;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class ApiClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @var string
     */
    private $newScormUrl = 'https://scorm-storage.test/scorm.zip';

    /**
     * @throws RefreshTokenFailedException
     * @throws SendWebhookFailedException
     */
    public function testNewScormWebhookNoTokenExists(): void
    {
        $newAccessToken = 'access_token_value';
        $newAccessTokenExpiresAt = '01-01-2021 12:00';

        $newScormUrl = 'https://scorm-storage.test/scorm.zip';

        $settingsRepositoryMock = new TestSettingsRepository(null, null);

        $requestHistory = [];
        $client = $this->getGuzzleClient([
            new Response(200, [], json_encode(['access_token' => $newAccessToken, 'expires_at' => $newAccessTokenExpiresAt])),
            new Response(200),
        ], $requestHistory);

        $apiClient = new ApiClient($settingsRepositoryMock, $client);
        $apiClient->sendNewScormWebhook($newScormUrl);

        $this->assertEquals($newAccessToken, $settingsRepositoryMock->getAccessToken());
        $this->checkResponse(
            $requestHistory[0],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/auth/login',
            [
                'client_id' => $settingsRepositoryMock->getClientId(),
                'client_secret' => $settingsRepositoryMock->getClientSecret(),
            ]
        );
        $this->checkResponse(
            $requestHistory[1],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/new_scorm',
            ['url' => $newScormUrl]
        );
    }

    public function testNewScormWebhookGot401Response(): void
    {
        $oldAccessToken = 'old_token';
        $newAccessToken = 'access_token_value';
        $newAccessTokenExpiresAt = '01-01-2021 12:00';

        $newScormUrl = 'https://scorm-storage.test/scorm.zip';

        $settingsRepositoryMock = new TestSettingsRepository($oldAccessToken, Carbon::now()->addYear());

        $requestHistory = [];
        $client = $this->getGuzzleClient([
            new Response(401),
            new Response(200, [], json_encode(['access_token' => $newAccessToken, 'expires_at' => $newAccessTokenExpiresAt])),
            new Response(200),
        ], $requestHistory);

        $apiClient = new ApiClient($settingsRepositoryMock, $client);
        $apiClient->sendNewScormWebhook($newScormUrl);

        $this->assertEquals($newAccessToken, $settingsRepositoryMock->getAccessToken());
        $this->checkResponse(
            $requestHistory[0],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/new_scorm',
            ['url' => $newScormUrl]
        );
        $this->checkResponse(
            $requestHistory[1],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/auth/login',
            [
                'client_id' => $settingsRepositoryMock->getClientId(),
                'client_secret' => $settingsRepositoryMock->getClientSecret(),
            ]
        );
        $this->checkResponse(
            $requestHistory[2],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/new_scorm',
            ['url' => $newScormUrl]
        );
    }

    public function testNewScormWebhookIsOk(): void
    {
        $newScormUrl = 'https://scorm-storage.test/scorm.zip';

        $settingsRepositoryMock = new TestSettingsRepository('access_token', Carbon::now()->addYear());

        $requestHistory = [];
        $client = $this->getGuzzleClient([
            new Response(200),
        ], $requestHistory);

        $apiClient = new ApiClient($settingsRepositoryMock, $client);
        $apiClient->sendNewScormWebhook($newScormUrl);

        $this->checkResponse(
            $requestHistory[0],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/new_scorm',
            ['url' => $newScormUrl]
        );
    }

    public function testDeletedScormWebhookNoTokenExists(): void
    {
        $newAccessToken = 'access_token_value';
        $newAccessTokenExpiresAt = '01-01-2021 12:00';

        $newScormUrl = 'https://scorm-storage.test/scorm.zip';

        $settingsRepositoryMock = new TestSettingsRepository(null, null);

        $requestHistory = [];
        $client = $this->getGuzzleClient([
            new Response(200, [], json_encode(['access_token' => $newAccessToken, 'expires_at' => $newAccessTokenExpiresAt])),
            new Response(200),
        ], $requestHistory);

        $apiClient = new ApiClient($settingsRepositoryMock, $client);
        $apiClient->sendScormDeletedWebhook($newScormUrl);

        $this->assertEquals($newAccessToken, $settingsRepositoryMock->getAccessToken());
        $this->checkResponse(
            $requestHistory[0],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/auth/login',
            [
                'client_id' => $settingsRepositoryMock->getClientId(),
                'client_secret' => $settingsRepositoryMock->getClientSecret(),
            ]
        );
        $this->checkResponse(
            $requestHistory[1],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/scorm_deleted',
            ['url' => $newScormUrl]
        );
    }

    public function testDeletedScormWebhookGot401Response(): void
    {
        $oldAccessToken = 'old_token';
        $newAccessToken = 'access_token_value';
        $newAccessTokenExpiresAt = '01-01-2021 12:00';

        $newScormUrl = 'https://scorm-storage.test/scorm.zip';

        $settingsRepositoryMock = new TestSettingsRepository($oldAccessToken, Carbon::now()->addYear());

        $requestHistory = [];
        $client = $this->getGuzzleClient([
            new Response(401),
            new Response(200, [], json_encode(['access_token' => $newAccessToken, 'expires_at' => $newAccessTokenExpiresAt])),
            new Response(200),
        ], $requestHistory);

        $apiClient = new ApiClient($settingsRepositoryMock, $client);
        $apiClient->sendScormDeletedWebhook($newScormUrl);

        $this->assertEquals($newAccessToken, $settingsRepositoryMock->getAccessToken());
        $this->checkResponse(
            $requestHistory[0],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/scorm_deleted',
            ['url' => $newScormUrl]
        );
        $this->checkResponse(
            $requestHistory[1],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/auth/login',
            [
                'client_id' => $settingsRepositoryMock->getClientId(),
                'client_secret' => $settingsRepositoryMock->getClientSecret(),
            ]
        );
        $this->checkResponse(
            $requestHistory[2],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/scorm_deleted',
            ['url' => $newScormUrl]
        );
    }

    public function testDeletedScormWebhookIsOk(): void
    {
        $newScormUrl = 'https://scorm-storage.test/scorm.zip';

        $settingsRepositoryMock = new TestSettingsRepository('access_token', Carbon::now()->addYear());

        $requestHistory = [];
        $client = $this->getGuzzleClient([
            new Response(200),
        ], $requestHistory);

        $apiClient = new ApiClient($settingsRepositoryMock, $client);
        $apiClient->sendScormDeletedWebhook($newScormUrl);

        $this->checkResponse(
            $requestHistory[0],
            $settingsRepositoryMock->getScormProxyUrl() . '/api/webhooks/scorm_deleted',
            ['url' => $newScormUrl]
        );
    }

    /**
     * @param array $mockResponses
     * @param array $requestHistory
     * @return Client
     */
    private function getGuzzleClient(array $mockResponses, array &$requestHistory): Client
    {
        $mock = new MockHandler($mockResponses);
        $handlerStack = HandlerStack::create($mock);
        $handlerStack->push(Middleware::history($requestHistory));
        return new Client(['handler' => $handlerStack]);
    }

    /**
     * @param array $requestArrayElement
     */
    private function checkResponse(array $requestArrayElement, string $expectedUrl, array $expectedBody): void
    {
        $request = $requestArrayElement['request'];
        $this->assertEquals($expectedUrl, $request->getUri()->__toString());
        $this->assertEquals($expectedBody, json_decode($request->getBody()->getContents(), true));
    }
}
