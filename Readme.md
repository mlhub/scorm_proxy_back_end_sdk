# Installation

## Install package with composer
`composer require mlh/scorm_proxy_back_end_sdk`

## Implement Interface IntegrationSettingsRepository
```php
<?php

use Carbon\Carbon;

class SettingsReposiotory implements \Mlh\ScormProxySdk\Configs\Interfaces\SettingsRepository
{
    public function getClientId(): string
    {
        return 'stored in your app client id';
    }

    public function getClientSecret(): string
    {
        return 'stored in your app client secret';
    }

    public function getScormProxyUrl(): string
    {
        return 'http://scorm-proxy.test';
    }

    public function getAccessToken(): string
    {
        return 'stored in your app access token';
    }

    public function setAccessToken(string $token, Carbon $expiresAt): void
    {
        // TODO: store somewhere given token
    }
}
```

## Register your implementation of IntegrationSettingsRepository in DI container of your app
```php
// laravel example
app()->bind(\Mlh\ScormProxySdk\Configs\Interfaces\SettingsRepository::class, \YourApp\SettingsRepositoryImplementation::class);
```

## (Not laravel only) Register all classes interfaces bindings in DI container of your app
```php
app()->bind(\Mlh\ScormProxySdk\Interfaces\ApiClient::class, \Mlh\ScormProxySdk\ApiClient::class);
```

# Basic Usage

## Send Webhooks

```php
use Mlh\ScormProxySdk\Interfaces\ApiClient;

class SomeClass
{
    /**
     * @var ApiClient 
     */
    private $apiClient;
    
    /**
     * @param ApiClient $urlManager
     */
    public function __construct(ApiClient $urlManager) 
    {
        $this->apiClient = $urlManager;
    }
    
    public function sendNewScormWebhook(): void 
    {
        $scormUrl = 'http://scorm-storage.test/scrom.zip';

        $this->apiClient->sendNewScormWebhook($scormUrl);
    }
    
    public function sendScormDeletedWebhook(): void 
    {
        $scormUrl = 'http://scorm-storage.test/scrom.zip';

        $this->apiClient->sendScormDeletedWebhook($scormUrl);
    }
}

```
